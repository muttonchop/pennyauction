# Adam Kucera
# items for static auction scraping

from scrapy.item import Item, Field

class AuctionItem(Item):
   auctionSite = Field()
   auctionID = Field()
   itemName = Field()
   auctionEndPrice = Field()
   date = Field()                # Auction end day
   time = Field()                # Auction end time
   valuePrice = Field()          # value of item as assigned by website
   rawBidHistory = Field()       # raw bidding history, set up in

   ## bidStuff = [('id0', bool), ('id1', bool), ... , ('id9', bool)]
   ## in-order for (username, bool for autobid) true for auto, false for manual
   
   ## for position, data in enumerate(bidStuff):
   ##    print position, data
