# Adam Kucera
# 1/28/2014
# spider to crawl pages for static end-auction data
import json, time
from scrapy.spider import BaseSpider
from scrapy.selector import Selector
from scrapy.http import FormRequest, Request
from scrapy import log
from allpayAuctions.items import AuctionItem


class QuibidsSpider(BaseSpider): # change to Spider for 0.22+ versions
    name = "quibidsStatic"
    allowed_domains = ["quibids.com"]
    download_delay = 2
    
    def start_requests(self):
        ajax_url = "http://www.quibids.com/ajax/spots.php"
        payload = {'a':'h',
                   'ea':'20',
                   'groupBy':'a',
                   'p':'0',
                   'sort':'endingsoon',
                   'tab':'c',
                   'type':'ending',
                   'v':'g'}
        for i in range(1,100): # expect 2000 auction entries
            payload['p'] = str(i)
            yield FormRequest(ajax_url, formdata=payload, callback = self.parse_recentlyCompleted)

    def parse_recentlyCompleted(self, response):
        """ takes each of the recently completed pages and extracts the individual auction links
        """
        data = json.loads(response.body)
        
        for auction in data['Auctions']:
            html = auction['html']
            sel = Selector(text=html)
            
            indv_auc_url_from_html = sel.xpath("//h5[@class='auction-item-title']//a/@href").extract()[0]
            indv_auc_fullUrl = 'http://www.quibids.com/en' + indv_auc_url_from_html
            
            yield Request("http://localhost:8050/render.html?url=%s" % indv_auc_fullUrl,
                          dont_filter = True,
                          callback = self.parse_items)

    def parse_items(self,response):
        """ Takes individual auctions and parses items out. Each response should be downloaded with the splash download middleware
        """
        sel = Selector(response)
        auctionID = response.url.split('-')[1]

        try:
            item = self.makeItem(sel)
            item['auctionID'] = auctionID
        except Exception as e:
            item = None
            self.log(str(e), level=log.DEBUG)
            self.log('Item creation error: %s' % response.url, level = log.WARNING)
        yield item
    
    def makeItem(self, sel):
        """ takes a Selector object and creates quibids item
        """
        
        ## begin data extraction on html ##
        bidHistory = sel.xpath("//table[@id='bid-history']/tbody/tr/td/text()").extract() # rows of table for bid history

        itemName = sel.xpath("//*[@id='product_title']/text()").extract()

        auctionEndPrice = bidHistory[1].strip('$')

        date_and_time = sel.xpath("//div[@id='end-time-disclaim']/p[2]/text()").extract()[0]
        date_and_time = date_and_time.split(' ')
        date_and_time.remove('') # remove empty space

        end_date = date_and_time[0]
        end_time = date_and_time[1] + ' ' + date_and_time[2] 

        valuePrice = sel.xpath("//ul[@class='price-breakdown']/li//span/text()").extract()[0]
        valuePrice = valuePrice.join(valuePrice.split())[1:] # strip all the nasty characters from valuePrice (stupid html)

        ## put information into item ##
        item = AuctionItem()
        item['auctionSite'] = 'quibids'
        item['itemName'] = itemName
        item['auctionEndPrice'] = auctionEndPrice
        item['date'] = end_date
        item['time'] = end_time
        item['valuePrice'] = valuePrice
        del bidHistory[1::3]
        item['rawBidHistory'] = bidHistory

        return item
