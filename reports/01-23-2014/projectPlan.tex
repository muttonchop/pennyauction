%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Adam Kucera
% project plan and requirements 
% for cs460 project 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[12pt]{article}
\usepackage{graphicx}
\usepackage{indentfirst}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{pgfgantt}
\usepackage[margin=1in]{geometry}

% Edit these as appropriate
\newcommand\yourname{Adam Kucera}                     % <-- your name
\newcommand\assigntitle{Project Requirements and Plan}% <-- assignment title

\thispagestyle{fancyplain}
\headheight 35pt
\lhead{\yourname}
\chead{\textbf{\Large \assigntitle}}
\rhead{January 23, 2014}
\headsep 10pt

\begin{document}
\section{Overview}
The purpose of this project is to explore the applications and implications of a popular, new online auction format termed \emph{penny auctions}. It is estimated that this project will take approximately fourteen weeks (one semester) to complete and will deliver an application that can display, in the simplest manner possible, the most advantageous online auction opportunities for the user.  

The requirements of an application to visualize a penny auction environment are dependent on the type of information one wishes to gather. Along these same lines, it also depends on the information available. With these two restrictions, the plan of execution is split up into three separate phases. The three phases will be: data collection, mathematical modeling and data integration, and an application to visualize the results. The first of these phases, data collection, is the most concrete in its requirements. However, each phase after that one will become more vague and dependent on the prior phase to dictate the concrete goals.

\section{Requirements}

\subsection{Phase I: Data Collection}
{\parindent0pt \emph{Goal}: To reliably collect and store all possible online auction data from completed auctions as well as full auction histories from live auctions.} 
\vspace{10pt}

The data collection phase will consist of creating a web-scraping program in Python to extract and store the individual quibids.com auction data. This data will be treated, and thus collected, in two different ways. The initial collection of data will be from recently completed auctions. The following set of data, called end-auction data, will be collected from the completed auctions:    
{\small \begin{description} \itemsep -2pt
     \item[Auction ID] A unique auction number
     \item[Item Name] A brief item description
     \item[Auction End Price]  The final price of the item
     \item[Date] Day the auction ended
     \item[Time] Time the auction ended
     \item[Value Price] The value of the item as assigned by QuiBids
     \item[Winner] The bidder ID of the winning bidder
     \item[Bid-O-Winner] Whether or not the auction was won by an automated bidder
     \item[Distinct Bidders] The number of distinct bidders in the last 10 bids
     \item[Distinct Bid-Os] The number of distinct bidders using automated bidders in the last 10 bids
     \item[Last Ten Bidders] The bidder IDs of the last ten bidders
  \end{description}}

This information only constitutes part of the whole picture. There is a lot of analytical value to be gained by observing an entire auction. This data, which will be used to evaluate more sophisticated elements of the auction in a later phase, is achieved by monitoring a single auction for its full duration. This data will include the following elements:
\newpage
{\small \begin{description} \itemsep -2pt
  \item[Auction ID] Uniquely identifies each auction
  \item[Bidder Username] Uniquely identifies each bidder (persists between auctions)
  \item[Bid Price] The new price of the item after this bid
  \item[BidOMatic] Whether or not this bid was placed through an automated routine or placed manually
  \item[Bidders in Last 5] The number of bidders in the last five minutes
  \item[Auction Clock] The time on the auction clock when this bid was placed
  \item[Auction Clock Reset] The time the auction clock resets to every time a new bid is placed 
  \item[Date] The date this bid was placed on 
  \item[Time] The time this bid was placed on
\end{description}}

\subsubsection{Miscellaneous Requirements}

To be able to utilize the data being collected this phase will also include implementing a mySQL server. This data will be sorted based on the Auction ID.      

Since the data being collected depends very highly on the structure of the website being scraped (Quibids.com in this case), I will have a suite of unit tests that will be able to ensure that the data I am collecting is what I expect and in the correct format. This is necessary to ensure good development practices as well as ensuring that the data collected isn't faulty or corrupt within the database. 

\subsection{Phase II: Mathematical Modeling and Real Time Data Integration}
{\parindent0pt \emph{Goal}: Design and implement hierarchical and game theoretic models to apply to static and dynamic auction data} 
\vspace{10pt}

The basic requirements of this phase would be to be able to generate several different graphical representations of the auction environment. This is advantageous for an informed user to be able to make informed choices. 

In addition to the basic report generating functions, this phase should also see the implementation of a basic hierarchical models. Previous studies have shown that relative accuracy can be achieved when predicting when an auction will end by using $k$-closest neighbor followed by Poisson regression. 

As an extension to the hierarchical model this phase should implement a more complex model to estimate the sophistication of individual users within an auction environment.  The model that should be used is a subperfect Nash equilibrium model. This model requires training with a specific data set before it can be used within the real time system. This training data can be a subset of the full auction data collected in phase one. 

A final requirement of phase II is the integration of real time data. The mathematical models briefly described above should be able to dynamically change their output as different data is being collected and analyzed. 

\subsection{Phase III: Visualization}
{\parindent0pt \emph{Goal}: Design an attractive user interface to display auction information.}

The final phase of the project is to create an environment for the user to most efficiently utilize the previous two phases. This interface should be laid out in a similar grid style as the Quibids website, but will have a heat map laid over each auction that will show the user, at a glance, which auctions are more likely to be won by placing a bid in the current time period. The application should interface with a basic version of the web scraper implemented in phase one to provide updates for the underlying models. Additional features could include the ability to generate reports based on auction category, item, or a user-defined criteria. 

\section{Project Plan}
  \begin{ganttchart}[y unit title=0.4cm,
      y unit chart=0.5cm,
      vgrid,hgrid,
      newline shortcut=true,
      title label anchor/.style={below=-1.6ex},
      title left shift=.05,
      title right shift=.05,
      title height=1,
      bar/.style={fill=gray!50},
      incomplete/.style={fill=white},
      progress label text={},
      bar height=0.7,
      group right shift=0,
      group top shift=.6,
      group height=.3,
      group peaks height =.2
    ]
    {1}{24}
    %labels
    \gantttitle{Project}{24} \\
    \gantttitle{Week 1}{4} 
    \gantttitle{Week 2}{4} 
    \gantttitle{Week 2}{4} 
    \gantttitle{Week 4}{4} 
    \gantttitle{Week 5}{4} 
    \gantttitle{Week 6}{4} \\
    %tasks
    
    \ganttbar{Project Proposal and docs}{1}{4} \\
    \ganttbar{Unit testing}{2}{8} \\
    \ganttbar{Database implementation}{6}{8} \\
    \ganttbar{End-auction data parsing}{8}{15} \\
    \ganttbar{Live auction data collection}{16}{24}
  \end{ganttchart}
\vspace{2.5in}
\begin{ganttchart}[y unit title=0.4cm,
      y unit chart=0.5cm,
      vgrid,hgrid,
      newline shortcut=true,
      title label anchor/.style={below=-1.6ex},
      title left shift=.05,
      title right shift=.05,
      title height=1,
      bar/.style={fill=gray!50},
      incomplete/.style={fill=white},
      progress label text={},
      bar height=0.7,
      group right shift=0,
      group top shift=.6,
      group height=.3,
      group peaks height =.2
    ]
    {1}{24}
    %labels
    \gantttitle{Project}{24} \\
    \gantttitle{Week 7}{4} 
    \gantttitle{Week 8}{4} 
    \gantttitle{Week 9}{4} 
    \gantttitle{Week 10}{4}
    \gantttitle{Week 11}{4}
    \gantttitle{Week 12}{4}\\
    %tasks
    \ganttbar{Hierarchical data model}{1}{4} \\
    \ganttbar{Game theoretic model}{4}{8} \\
    \ganttbar{Realtime data integration}{1}{8} \\
    \ganttbar{Design user environment}{9}{11} \\
    \ganttbar{Implement user interface}{12}{16} \\
    \ganttbar{Incorporate data into interface}{17}{24}
  \end{ganttchart}
\newpage
\centering
\includegraphics[natheight=430px, natwidth=1330px, height=\paperheight, width=\paperwidth, keepaspectratio, angle=90]{project.png}

\end{document}

